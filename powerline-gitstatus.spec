Name:           powerline-gitstatus
Version:        1.2.1
Release:        1%{?dist}
Summary:        A Powerline segment for showing the status of a Git working copy 

License:        MIT
URL:            https://pypi.org/project/powerline-gitstatus
Source0:        %pypi_source

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires:       python3
Requires:       powerline

%description
A Powerline segment for showing the status of a Git working copy.

By Jasper N. Brouwer.

It will show the branch-name, or the commit hash if in detached head state.

It will also show the number of commits behind, commits ahead, staged files,
unmerged files (conflicts), changed files, untracked files and stashed files if
that number is greater than zero.


%prep
%autosetup


%build
%py3_build


%install
%py3_install


%files
%{python3_sitelib}/*

%changelog
* Thu Aug 30 2018 Emanuele Di Giacomo <emanuele@digiacomo.cc> - 1.2.1-1
- First release
