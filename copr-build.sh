#!/bin/bash
COPR=edigiacomo/misc
set -ex
tmpdir=$(mktemp -d)
trap "rm -rf $tmpdir" EXIT
spectool -g -A -C $tmpdir powerline-gitstatus.spec
rpkg srpm --outdir $tmpdir --spec powerline-gitstatus.spec
copr-cli build --nowait $COPR $tmpdir/*rpm
